from flask import Flask, render_template,request
from scipy.misc import imsave, imread, imresize
import numpy as np
import keras.models
import re
import sys 
import os
sys.path.append(os.path.abspath("./model"))
from load import *
import base64

app = Flask(__name__)

global model, graph


import tensorflow as tf
graph = tf.get_default_graph()
from keras.models import model_from_json
json_file = open('model.json','r')
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json)
model.load_weights("model.h5")






def convertImage(imgData1):
	imgstr = re.search(b'base64,(.*)',imgData1).group(1)
	
	with open('output.png','wb') as output:
		output.write(base64.b64decode(imgstr))
	

@app.route('/')
def index():
	return render_template("index.html")

@app.route('/predict/',methods=['GET','POST'])
def predict():
	imgData = request.get_data()
	convertImage(imgData)
	x = imread('output.png',mode='L')
	x = np.invert(x)
	x = imresize(x,(32,32))
	x = x.reshape(1,32,32,1)
	with graph.as_default():
		
		out = model.predict(x)
		print(out)
		print(np.argmax(out,axis=1))
		response = np.array_str(np.argmax(out,axis=1))
		return response	
	

if __name__ == "__main__":
	port = int(os.environ.get('PORT', 5000))
	app.run(host='127.0.0.1', port=port)
	#app.run(debug=True)
